# bifranteco

**Bibliographie francophone sur TeX et (logiciels) compagnons**

## Principes

Bibliographie des ouvrages écrits en français traitant de TeX et des
logiciels compagnons.

### Fichiers et formats de fichiers

Format de la bibliographie : `biblatex`

Codage des fichiers : utf-8.

Un fichier `.bib` (nommé ici *notice*) par ouvrages, un script lua
pour aggréger toutes les notices en un seul fichier `.bib` dont le nom
est `bifranteco.bib`[^1].

Images (jpg) de la couverture. L'original est scanné à 600ppp,  les
fichiers dont le nom se termine par 005 (resp. 050) sont des
réductions à 5% (resp. 50%) de l'original.

## Structure de la bibliographie

On peut utiliser tous les types d'entrée et tous les champs idoines
fournis par `biblatex`. Le manuel de la dernière version au moment de
la rédaction de la notice fait foi.

On y ajoute des champs spécifiques.

### Champs spécifiques à cette bibliographie

Les noms des champs spécifiques commencent par BFT. 

*  BFTauthordescr : description de l'auteur au moment de la publication ;
*  BFTauthordescrsrc : source de la description, p. ex. couv4
*  BFTindex : nombre d'index
*  BFTbiblio : nombre de bibliographies
*  BFTlistes : nombre de listes
*  BFTcontrole : auteur de la notice, date de la notice, source de la
   notice
*  BTFsudoc : identifiant pérenne de la notice du catalogue [Sudoc(abes)](http://www.sudoc.abes.fr/cbs/)
*  BTFbnfnotice : numéro de la notice BNF [Gallica](https://gallica.bnf.fr)
*  BTFbnfurl : url de la notice sur Gallica (ark) [identifiants
   BNF](http://api.bnf.fr/les-identifiants-pivots-entre-les-api) 
*  BTFworldcat : identifiant sur [worldcat](https://www.worldcat.org/) (Numéro OCLC)
  

Si un champ BFTindex / biblio / liste n'est pas nul, on donnera, dans l'ordre
d'apparition dans l'ouvrage, le type d'index / biblio / liste dans les
champs du type BFTindexI, BFTindexII, etc.  Le texte contenu dans ces
champs devrait pouvoir former un énoncé grammatical avec les mots index,
bibliographie, liste comme, p. ex. « index des concepts », « index des
commandes », etc.

## Fichiers 

Pour chaque ouvrage, on a une notice (fichier `.bib`) dans le dossier
`bibnotices`.  Le nom de ce fichier est identique à la clé de
bibliographie. Il est composé du nom de l'auteur suivi de l'année de
parution de l'ouvrage. Dans le cas d'auteurs multiples, on joint les
noms par deux tirets.

On a donc, p. ex. `desgraupes1999.bib`, pour un ouvrage écrit par Desgraupes
et paru en 1999, et `bitouze--charpentier2010`, pour un ouvrage co-écrit par
Denis Bitouzé et Jean-Côme Charpentier.

[^1]: Des voix se sont élevés pour que le nom change. La décision
    reste à prendre.
